import { getSecret } from '../utils/secrets-storage';
import { SecretError } from '../utils/custom-errors';

const jwt = require('jsonwebtoken');

const decryptToken = token => new Promise(async (resolve, reject) => {
  const secret = await getSecret('super-secret');

  jwt.verify(token, secret, (err, decoded) => {
    if (err) {
      reject(new SecretError('Fail decrypt Token'));
    }
    resolve(decoded);
  });
});

export { decryptToken };
