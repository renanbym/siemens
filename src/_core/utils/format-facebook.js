
const request = require('request');
const axios = require('axios');
const https = require('https');

import { getSecret } from './secrets-storage';


const FacebookFormat = {

    sendAPI: async (payload) => {
        const access_token = await getSecret('access-token-fb');
        const url = `https://graph.facebook.com/v3.2/me/messages?access_token=${access_token}`;

        return await axios.post(url, payload);
    }

    , sendAPI2: async (messageData) => {
        console.log('sendAPI2')
        // const access_token = await getSecret('access-token-fb');

        console.log(messageData)
        const access_token = 'EAAEHYvqL5AQBAF9iieTvGm1z1q5DXIquG9a93CeQZBMhVkZAhECiths8ZAhETFJCG4HPrY2vj3QOZACAXjjc9LqMbYVY8kz3fSESKZBknZCWLTnswDm0YnIagd8GvHYnzDsZCDXXLy8XJcn3glvsjNeVvPb2mgGDI6kdkXCpZAkpWZCAhZAZCIBqmvl';

        return new Promise((resolve, reject) => {

            request({
                uri: 'https://graph.facebook.com/v3.2/me/messages',
                qs: { access_token: access_token },
                method: 'POST',
                json: messageData
            }, (error, response, body) => {

                console.log(body);
                if (response.statusCode == 200) {
                    resolve(body);
                } else {
                    reject(body);
                }

            });

        })
    }

    , sendTextMessage: async (recipientId, messageText) => {
        console.log('sendTextMessage', messageText)
        let messageData = {
            recipient: {
                id: recipientId
            },
            message: {
                text: messageText
            }
        };

        const result = await FacebookFormat.sendAPI(messageData);
        return result;
    }

    , sendImageMessage: (recipientId, messageImage) => {
        let messageData = {
            recipient: {
                id: recipientId
            },
            message: {
                attachment: {
                    type: "image",
                    payload: {
                        url: messageImage
                    }
                }
            }
        };
        return FacebookFormat.sendAPI(messageData);
    }

    , sendButtonMessage: async (recipientId, params) => {
        let messageData = {
            recipient: {
                id: recipientId
            },
            message: {
                attachment: {
                    type: "template",
                    payload: {
                        template_type: "button",
                        text: params.title,
                        buttons: params.buttons.map((c) => { c.type = "postback"; return c; })
                    }
                }
            }
        };

        return FacebookFormat.sendAPI(messageData);
    }

    , sendQuickMessage: async (recipientId, params) => {

        let messageData = {
            recipient: {
                id: recipientId
            },
            message: {
                text: params.title,
                quick_replies: params.quick_replies.map((c) => { c.content_type = "text"; return c; })
            }
        };
        return FacebookFormat.sendAPI(messageData);

    }

    , sendAction: async (recipientId, params) => {
        let messageData = {
            recipient: {
                id: recipientId
            },
            sender_action: params.action
        };

        return FacebookFormat.sendAPI(messageData);
    }

    , sendGenericMessage: async (recipientId, params) => {

        let text_postback = typeof params.text_payload_generic !== 'undefined' ? params.text_payload_generic : "More info"

        let elements = params.lists.map((c) => { let a = {}; a.title = c.title; a.image_url = c.image_url; a.buttons = [{ type: "postback", title: text_postback, payload: c.payload }]; return a; })

        let messageData = {
            recipient: {
                id: recipientId
            },
            message: {
                attachment: {
                    type: "template",
                    payload: {
                        template_type: "generic",
                        text: params.title,
                        image_aspect_ratio: "square",
                        elements: elements
                    }
                }
            }
        };



        return FacebookFormat.sendAPI(messageData);
    }

    , sendGenericButtonMessage: async (recipientId, params) => {

        let elements = {
            title: params.question
            , image_url: params.image
            , buttons: []
        }
        params.answers.map((c) => { elements.buttons.push({ type: "postback", title: c.title, payload: c.payload }) })

        let messageData = {
            recipient: {
                id: recipientId
            },
            message: {
                attachment: {
                    type: "template",
                    payload: {
                        template_type: "generic",
                        image_aspect_ratio: "square",
                        elements: [elements]
                    }
                }
            }
        };



        return FacebookFormat.sendAPI(messageData);
    }


    , sendAttachmentsMessage: async (recipientId, params) => {
        let payload = {}

        if (params.url) {
            payload.url = params.url;
            payload.is_reusable = 'true';
        }

        if (params.attachment_id) payload.attachment_id = params.attachment_id;

        let messageData = {
            recipient: {
                id: recipientId
            },
            message: {
                attachment: {
                    type: params.type,
                    payload: payload
                }
            }
        };
        return FacebookFormat.sendAPI(messageData);
    }

}

export { FacebookFormat };
