
import { DynamoCreate, DynamoGet } from 'services/class/DynamoDB';
import { ElasticSearchDocuments } from 'services/class/ElasticSearch';
import { ErrorHandler } from '_core/utils/error-handling';
import { FacebookFormat } from '_core/utils/format-facebook';
import { success } from '_core/utils/response';
import { getSecret } from '_core/utils/secrets-storage';

const axios = require('axios');
const moment = require('moment');


const search = async (senderID, text) => {

  const meetups = await ElasticSearchDocuments(
    'siemens', '_doc',
    {
      query: {
        match: {
          'event.event_name': text
        }
      }
    }
  );

  if (meetups.hits.total) {
    const btns = meetups.hits.hits.map(cur => ({ title: cur._source.event.event_name, payload: cur._source.event.event_id })).slice(0, 3);

    const params = {
      title: `I have the following ${meetups.hits.total} Meetups in my database${meetups.hits.total > 3 ? ', but I can only show the next 3.' : '.'}`,
      buttons: btns
    };
    await FacebookFormat.sendButtonMessage(senderID, params);

  } else {
    await FacebookFormat.sendTextMessage(senderID, ':( I have not found any events with these terms, please try again later.');
  }


};

const receivedMessage = async (event) => {

  const senderID = event.sender.id;
  const recipientID = event.recipient.id;
  const timeOfMessage = event.timestamp;

  const message = event.message;
  const messageId = message.mid;

  const messageText = message.text;
  const messageAttachments = message.attachments;


  const user = await DynamoGet({
    idUser: senderID,
    createdAt: moment().format('YYYY-MM-DD')
  });

    /**
     * New Chat
     */
  if (!user) {
    await initChat(senderID);
    await search(senderID, messageText);
  } else {
    // await FacebookFormat.sendTextMessage(senderID, "What meetup are you interested in?")
    await search(senderID, messageText);
  }
};

const initChat = async (senderID) => {
  console.log('initChat');

  const user = await DynamoCreate({ idUser: senderID });
  await FacebookFormat.sendTextMessage(senderID, 'Welcome to Meetup Bot');
  await FacebookFormat.sendTextMessage(senderID, 'What meetup are you interested in?');
};

async function receivedPostback(event) {
  const senderID = event.sender.id;
  const recipientID = event.recipient.id;
  const timeOfPostback = event.timestamp;
  const payload = event.postback.payload;

  console.log('receivedPostback', payload);

  const tokenMeetup = await getSecret('api-meetup');

  if (payload === 'GET_STARTED_PAYLOAD') {
    await initChat(senderID);
  } else {

    const event = payload;
    const url = `https://api.meetup.com/2/event/${event}/?key=${tokenMeetup}`;
    console.log(url);
    const info = await axios.get(url);
    console.log(info.data);
    await FacebookFormat.sendTextMessage(senderID, `For the '${info.data.name}' there are currently ${info.data.yes_rsvp_count} users attending.`);
  }

}

const handler = async (event, context, callback) => {

  context.callbackWaitsForEmptyEventLoop = false;
  if (event.source === 'serverless-plugin-warmup') {
    console.log('WarmUP - Lambda is warm!');
    return callback(null, 'Lambda is warm!');
  }

  try {

    const data = JSON.parse(event.body);

    if (data.object === 'page') {

      if (typeof data.entry !== 'undefined' && data.entry.length > 0) {

        for (const entry of data.entry) {

          for (const messagingItem of entry.messaging) {

            if (messagingItem.message && messagingItem.message.text) {
              await receivedMessage(messagingItem);
            } else if (messagingItem.postback) {
              await receivedPostback(messagingItem);
            } else {
              console.error('Webhook received unknown messagingItem');
            }
          }
        }
      }

      return callback(null, success({}));

    }

  } catch (e) {
    const errorMessage = await ErrorHandler(e, event, context);
    return callback(null, errorMessage);
  }
};

export { handler };

