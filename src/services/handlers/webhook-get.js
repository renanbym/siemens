
import { ItemNotFoundError } from '_core/utils/custom-errors';
import { ErrorHandler } from '_core/utils/error-handling';
import { text } from '_core/utils/response';

const handler = async (event, context, callback) => {

  context.callbackWaitsForEmptyEventLoop = false;
  if (event.source === 'serverless-plugin-warmup') {
    console.log('WarmUP - Lambda is warm!');
    return callback(null, 'Lambda is warm!');
  }

  try {

    if (event.queryStringParameters['hub.mode'] === 'subscribe' && event.queryStringParameters['hub.verify_token'] === 'siemens-dev-chatbot') {
      return callback(null, text(event.queryStringParameters['hub.challenge']));
    }

    throw new ItemNotFoundError();


  } catch (e) {
    const errorMessage = await ErrorHandler(e, event, context);
    return callback(null, errorMessage);
  }
};

export { handler };

