
import { ElasticCreateDocument } from 'services/class/ElasticSearch';
import { ErrorHandler } from '_core/utils/error-handling';
import { success } from '_core/utils/response';

const handler = async (event, context, callback) => {


  try {
    // console.log(event);
    for (const record of event.Records) {
      const payload = JSON.parse(new Buffer(record.kinesis.data, 'base64').toString('ascii'));
      console.log('RSVP:', payload.rsvp_id, payload.event.event_id, payload.event.event_name);

      await ElasticCreateDocument('siemens', '_doc', payload);
    }
    return callback(null, success({ pong: 'kinesis: pong' }));
  } catch (e) {
    const errorMessage = await ErrorHandler(e, event, context);
    return callback(null, errorMessage);
  }
};

export { handler };

