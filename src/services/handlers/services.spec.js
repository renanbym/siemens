
const LambdaTester = require('lambda-tester').noVersionCheck();
const expect = require('chai').expect;

const kinesis = require('./kinesis').handler;
const ping = require('./ping').handler;
const webhookGet = require('./webhook-get').handler;
const webhookPost = require('./webhook-post').handler;

describe('Services: ', () => {


  it('pong: should answer status code 200', () => LambdaTester(ping)
    .event({})
    .expectResult((data) => {

      expect(data.statusCode).to.exist;
      expect(data.statusCode).to.equal(200);

    }));


  it('webhookGet: should answer status code 200', () => LambdaTester(webhookGet)
    .event({
      queryStringParameters: {
        'hub.mode': 'subscribe',
        'hub.verify_token': 'siemens-dev-chatbot',
        'hub.challenge': 'teste'
      }
    })
    .expectResult((data) => {

      expect(data.statusCode).to.exist;
      expect(data.statusCode).to.equal(200);

    }));


  it('webhookPost: should answer status code 200', () => LambdaTester(webhookPost)
    .event({
      body: JSON.stringify({
        object: 'page'
      })
    })
    .expectResult((data) => {

      expect(data.statusCode).to.exist;
      expect(data.statusCode).to.equal(200);

    }));


});
