const AWS = require('aws-sdk');

const dynamoDb = new AWS.DynamoDB.DocumentClient();

const table = 'siemens-dev';
const uuid = require('uuid');


const moment = require('moment');

const DynamoCreate = async (data) => {

    const timestamp = moment().format('YYYY-MM-DD');

    const params = {
        TableName: table,
        Item: Object.assign({ id: uuid.v1(), createdAt: timestamp }, data)
    };

    return new Promise((resolve, reject) => {
        dynamoDb.put(params, (error, result) => {
            if (error) reject(error)
            resolve(result);
        });

    })

}

const DynamoGet = async (keys) => {
    const params = {
        TableName: table,
        Key: keys
    };

    return new Promise((resolve, reject) => {

        dynamoDb.scan(params, (error, result) => {
            if (error) reject(error)
            resolve(result.Items[0]);

        });

    })
}



export { DynamoCreate, DynamoGet }