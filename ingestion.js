const WebSocket = require('ws');
const AWS = require('aws-sdk');

const credentials = new AWS.SharedIniFileCredentials({ profile: 'siemens-dev' });

const kinesis = new AWS.Kinesis({
  region: 'us-east-1',
  credentials
});

AWS.config.credentials = credentials;

const putKinises = (data) => {


  const params = {
    Records: [
      {
        Data: data,
        PartitionKey: `siemens-${Math.floor(Math.random() * 100000)}`
        // ExplicitHashKey: 'STRING_VALUE'
      }
    ],
    StreamName: 'siemens-meetup-dev'
  };

  kinesis.putRecords(params, (err, data) => {
    if (err) console.log(err, err.stack); // an error occurred
    else console.log(data); // successful response
  });


};

try {

  const ws = new WebSocket('ws://stream.meetup.com/2/rsvps', {
    perMessageDeflate: false
  });


  ws.on('message', (data) => {

    const { rsvp_id, event } = JSON.parse(data);
    const request_data = JSON.stringify({ rsvp_id, event });
    putKinises(request_data);

  });

} catch (e) {
  console.log(e);
}
